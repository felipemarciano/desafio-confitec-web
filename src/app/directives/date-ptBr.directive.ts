import { isNullOrWhiteSpace } from 'src/app/shared/modules/help-module';
import { Directive, ElementRef, HostListener } from '@angular/core';
import * as moment from 'moment';

@Directive({
  selector: '[datePtBr]'
})
export class DateDirective{

  constructor(private elemento: ElementRef) {

  }

  @HostListener('keyup')
  @HostListener('keydown')
  onkeyUp() {
    let data: string = this.elemento.nativeElement.value;
    if (!isNullOrWhiteSpace(data)) {
        let v = data.replace(/\D/g,'').slice(0, 10);
        if (v.length >= 5) {
          v = `${v.slice(0,2)}/${v.slice(2,4)}/${v.slice(4)}`;
        }
        else if (v.length >= 3) {
          v = `${v.slice(0,2)}/${v.slice(2)}`;
        }
        this.elemento.nativeElement.value = v;
        //this._renderer.setValue(this.elemento.nativeElement, v);
        //this._renderer.setValue(this.elemento.nativeElement, moment(data).format("DD/MM/YYYY"));
    }
  }
}
