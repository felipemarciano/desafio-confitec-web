import { isNullOrWhiteSpace } from 'src/app/shared/modules/help-module';
import { Directive, ElementRef, Input, OnInit, Renderer2 } from '@angular/core';

@Directive({
  selector: '[alinhar]'
})
export class AlinharDirective implements OnInit{
  @Input("alinhar") _alinhar: string;

  constructor(private elemento: ElementRef, private _renderer: Renderer2) {

  }

  ngOnInit(){
    if(isNullOrWhiteSpace(this._alinhar)){
      this._alinhar = 'right';
    }

    this._renderer.setStyle(this.elemento.nativeElement, 'text-align', this._alinhar);
  }
}
