import { NgModule } from '@angular/core';
import { AlinharDirective } from './styles/alinhar/alinhar.directive';
import { DateDirective } from './date-ptBr.directive';

@NgModule({
	declarations: [
		AlinharDirective,
    DateDirective
	],
	exports: [
		AlinharDirective,
    DateDirective
  ]
})
export class DirectivesModule {}
