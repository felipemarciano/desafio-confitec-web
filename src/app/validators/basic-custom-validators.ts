import { FormControl, AbstractControl, ValidatorFn } from '@angular/forms';
import { isNullOrUndefined } from '../shared/modules/help-module';

export class BasicCustomValidators {

  static email(control: FormControl): { [key: string]: any } {
    var emailRegexp = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/;
    if (control.value && !emailRegexp.test(control.value)) {
      return { 'email' : true };
    }
  }

  static required(control: AbstractControl): { [key: string]: any } | null {
    const controlIsValid = control?.value !== undefined &&
      control.value !== 'undefined' &&
      control.value !== null &&
      control.value !== 'null';

    return controlIsValid ? null : { 'required': true };
  }

  static onlyNumbers(control: AbstractControl): { [key: string]: any } | null {
    if (isNullOrUndefined(control)) {
      return null;
    }

    const reg = new RegExp('/-?[\d\.]+/g');
    const controlIsValid = reg.test(control.value);

    return controlIsValid ? null : { 'onlyNumbers': true };
  }

  static hasLength(hasLength: number): ValidatorFn {

    return (control: AbstractControl) => {
      if (isNullOrUndefined(control?.value)) {
        return null;
      }

      const controlIsValid = control.value.length === hasLength;

      return controlIsValid ? null : { 'hasLength': true };
    }
  }

  static range(minLength: number, maxLength: number): ValidatorFn {

    return (control: AbstractControl) => {
      if (isNullOrUndefined(control?.value)) {
        return null;
      }

      const controlIsValid = control.value.length >= minLength && control.value.length <= maxLength;

      return controlIsValid ? null : { 'range': true };
    }
  }

  static equal(controlCompare: FormControl): ValidatorFn {

    return (control: AbstractControl) => {
      if (isNullOrUndefined(control)) {
        return null;
      }

      if (isNullOrUndefined(control.value) && isNullOrUndefined(controlCompare.value)) {
        return { 'equal': true };
      }

      return control.value === controlCompare.value ? null : { 'equal': true };
    }
  }
}
