import { UsuarioTokenService } from './pages/usuarios/shared/services/usuario-token.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler, CUSTOM_ELEMENTS_SCHEMA, LOCALE_ID } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { HeaderComponent } from './pages/header/header.component';
import { HomeComponent } from './pages/home/home.component';
import { ErrorLogService } from './shared/error-handler/error-log.service';
import { AuthUsuarioService } from './pages/usuarios/shared/services/auth.usuario.service';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule, registerLocaleData } from '@angular/common';
import pt from '@angular/common/locales/pt';
import { RouterModule } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';
import { AcessoNegadoComponent } from './pages/acesso-negado/acesso-negado.component';
import { NgxSpinnerModule, NgxSpinnerService } from "ngx-spinner";
import { AppRoutingModule } from './app-routing.module';
import { GlobalErrorHandler } from './shared/error-handler/global-error-handler';
import { DirectivesModule } from './directives/directives.module';
import { PipesModule } from './pipes/pipes.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { BreadcrumbComponent } from './pages/breadcrumb/breadcrumb.component';
import { UsuarioService } from './pages/usuarios/shared/services/usuario.service';
import { EscolaridadeService } from './pages/escolaridades/shared/escolaridade.service';
import { LogOutComponent } from './pages/logout/logout.component';

registerLocaleData(pt, 'pt');
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    LoginComponent,
    AcessoNegadoComponent,
    BreadcrumbComponent,
    LogOutComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule,
    HttpClientModule,
    BrowserAnimationsModule,
    NgxSpinnerModule,
    FormsModule,
    ReactiveFormsModule,
    ToastrModule.forRoot({
      timeOut: 10000,
      positionClass: 'toast-top-right',
      preventDuplicates: true,
      closeButton: true,
      autoDismiss: true
    }),
    DirectivesModule,
    PipesModule,
    AppRoutingModule
  ],
  providers: [
    ErrorLogService,
    GlobalErrorHandler,
    UsuarioService,
    AuthUsuarioService,
    ToastrService,
    NgxSpinnerService,
    EscolaridadeService,
    UsuarioTokenService,
    { provide: LOCALE_ID, useValue: 'pt' },
    { provide: ErrorHandler, useClass: GlobalErrorHandler },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_LOCALE, useValue: 'pt'},
    {
      provide: MAT_DATE_FORMATS, useValue: {
        parse: {
          dateInput: ['L','LL'],
        },
        display: {
          dateInput: 'L',
          monthYearLabel: 'MMM YYYY',
          dateA11yLabel: 'LL',
          monthYearA11yLabel: 'MMMM YYYY',
        },
      },
    },
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },
    { provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: { strict: true, useUtc: true } }
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  bootstrap: [AppComponent]
})
export class AppModule { }
