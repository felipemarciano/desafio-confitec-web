import { isNullOrWhiteSpace } from 'src/app/shared/modules/help-module';
import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';

@Pipe({
  name: 'dateTimePtBr',
})
export class DateTimePtBrPipe implements PipeTransform {
  constructor(private datePipe: DatePipe) {

  }

  transform(value: string, somenteData:boolean = false):string {
    if(isNullOrWhiteSpace(value)){
      return;
    }
    if(somenteData)
      return this.datePipe.transform(value, "dd/MM/yyyy");

      return this.datePipe.transform(value, "dd/MM/yyyy HH:mm:ss");
  }
}
