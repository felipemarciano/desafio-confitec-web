import { NgModule } from '@angular/core';
import { CommonModule, DatePipe, CurrencyPipe, DecimalPipe } from '@angular/common';
import { DateTimePtBrPipe } from './date/date-pt-br.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    DateTimePtBrPipe
  ],
  exports: [
    DateTimePtBrPipe
  ],
  providers: [
    DatePipe,
    CurrencyPipe,
    DecimalPipe
  ]
})
export class PipesModule { }
