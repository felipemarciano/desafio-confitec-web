import { BaseModel } from 'src/app/shared/models/base.model';

export class Escolaridade extends BaseModel{

  public Descricao: string;

  static fromJson(jsonData: any): Escolaridade {
    return Object.assign(new Escolaridade(), jsonData);
  }
}
