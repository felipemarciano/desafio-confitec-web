import { Escolaridade } from './escolaridade.model';
import { BaseService } from 'src/app/services/base.service';
import { Injectable, Injector } from '@angular/core';

@Injectable()
export class EscolaridadeService extends BaseService<Escolaridade> {

  constructor(injector: Injector) {
    super("escolaridades", injector, Escolaridade.fromJson);
  }
}
