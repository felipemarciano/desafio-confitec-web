
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BreadcrumbParent } from '../breadcrumb/models/breadcrumb-parent.model';
import { AuthUsuarioService } from '../usuarios/shared/services/auth.usuario.service';
import { SobreComponent } from './sobre.component';

const breadcrumb: BreadcrumbParent[]  = [{id: 1, name: 'Sobre', routerLink: '/sobre'}];

const routes: Routes = [
  { path: '', component: SobreComponent, canActivate : [AuthUsuarioService], pathMatch: 'full', data: { breadcrumb: {name: breadcrumb[0].name} } },
];

@NgModule({
  declarations: [
    SobreComponent,
  ],
  imports: [
    RouterModule.forChild(routes)
  ]
})
export class SobreModule { }
