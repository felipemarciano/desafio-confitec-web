export class BreadcrumbParent{
  constructor(public id: number,
              public name: string,
              public routerLink: string,
              public icon?: string) {

  }
}
