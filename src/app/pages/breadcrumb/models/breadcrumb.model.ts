import { BreadcrumbParent } from './breadcrumb-parent.model';

export class Breadcrumb{
  constructor(public name: string,
              public parents: BreadcrumbParent[]
              )
              {}
}



