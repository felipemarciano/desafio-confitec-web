import 'src/app/shared/extensions/array.extension';
import { Component, ViewEncapsulation } from '@angular/core';
import { Router, ActivationStart } from '@angular/router';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class BreadcrumbComponent {

  public _breadcrumb: any;

  constructor(private _router: Router) {

    _router.events.subscribe(routerEvent => {
      this.verifyRouter(routerEvent);
    });
  }

  verifyRouter(routerEvent: any): void {
    if (routerEvent instanceof ActivationStart) {
      if (routerEvent.snapshot.data) {
        this._breadcrumb = routerEvent.snapshot.data['breadcrumb'];
        if(this._breadcrumb && this._breadcrumb.parents){
          this._breadcrumb.parents.orderBy('id');
        }
      }
    }
  }
}
