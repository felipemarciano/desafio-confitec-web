import { UsuarioTokenService } from './../usuarios/shared/services/usuario-token.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsuarioService } from '../usuarios/shared/services/usuario.service';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html'
})
export class LogOutComponent implements OnInit {

  constructor(
    private _router: Router,
    private _usuarioTokenService: UsuarioTokenService,
    private _usuarioService: UsuarioService,
    private _ngxSpinnerService: NgxSpinnerService
  ) {
  }

  ngOnInit(): void {
    this._ngxSpinnerService.show();
    setTimeout(() => {
      this._usuarioService.logOut()
      .subscribe(data => {
        console.log(JSON.stringify(data))
        this._usuarioTokenService.removeUserToken();
      });
      this._ngxSpinnerService.hide();
      this._router.navigate(['/login']);
    }, 1000)
  }
}
