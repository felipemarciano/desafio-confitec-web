export class AutenticarUsuario{
    public Email: string;
    public Nome: string;

    static validacoes() {
        return {
            Email:{
                required: "Informe o email!",
                email: "Informe um email válido!"
            },
            Senha: {
                required: "Informe a senha",
            }
        };
    }
}