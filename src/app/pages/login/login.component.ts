import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { BaseResult } from 'src/app/shared/models/base-result.model';
import { TokenLogin } from 'src/app/pages/usuarios/shared/models/token-login.model';
import * as $ from 'jquery';
import { Router } from '@angular/router';
import { GlobalErrorHandler } from 'src/app/shared/error-handler/global-error-handler';
import { BasicCustomValidators } from 'src/app/validators/basic-custom-validators';
import { UsuarioService } from '../usuarios/shared/services/usuario.service';
import { UsuarioTokenService } from '../usuarios/shared/services/usuario-token.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {
  public form: FormGroup;

  constructor(
    public fb: FormBuilder,
    public router: Router,
    private _usuarioTokenService: UsuarioTokenService,
    private _usuarioService: UsuarioService,
    private _globalErrorHandler: GlobalErrorHandler,
    private _ngxSpinnerService: NgxSpinnerService
  ) {
    this.form = this.fb.group({
      Email: [null, Validators.compose([BasicCustomValidators.required, BasicCustomValidators.email])],
      Senha: [null, Validators.compose([BasicCustomValidators.required])]
    });
  }

  ngOnInit(): void {
    $(document).ready(function () {
      $('#Email').focus();
    });
  }

  public onSubmit(): void {
    if (this.form.valid) {
      this._ngxSpinnerService.show();
      const email = this.form.controls['Email'].value;
      const senha = this.form.controls['Senha'].value;

      this._usuarioService.login({ Email: email, Senha: senha })
        .subscribe((result: BaseResult) => {
          this._ngxSpinnerService.hide();
          if (!result.Success) {
            this._globalErrorHandler.handleError(result);
            return;
          }

          const tokenResult = result.Data as TokenLogin;
          if (tokenResult) {
            this._usuarioTokenService.setLocalStorage(tokenResult);
            this.router.navigate(['/']);
          }
        }, error => {
          this._ngxSpinnerService.hide();
          this._globalErrorHandler.handleError(error);
        });
    }
  }
}
