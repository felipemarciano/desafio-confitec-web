export class RegistrarUsuario {
    public ConfirmacaoSenha: string;
    public Email: string;
    public Nome: string;
    public Senha: string;

    static fromJson(jsonData: any): RegistrarUsuario {
      return Object.assign(new RegistrarUsuario(), jsonData);
    }

    static validacoes() {
        return {
            Nome: {
                required: "Informe o nome!",
                minlength: "Informe no mínimo 3 caracteres!",
                maxlength: "Informe no máximo 20 caracteres!"
            },
            Email:{
                required: "Informe o email!",
                maxlength: "Informe no máximo 80 caracteres!",
                email: "Informe um email válido!"
            },
            Senha: {
                required: "Informe a senha com ao menos 1 letra e 1 número!",
                minlength: "Informe a senha com no mínimo 4 caracteres!",
                maxlength: "Informe a senha com no máximo 10 caracteres!"
            },
            ConfirmacaoSenha: {
                required: "Repita a senha!",
                minlength: "Repita a senha com no mínimo 4 caracteres!",
                maxlength: "Repita a senha com no máximo 10 caracteres!",
                equal: "As senhas não conferem!"
            }
        };
    }
}
