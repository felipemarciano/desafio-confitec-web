import { Component, OnInit, Injector, AfterViewInit } from '@angular/core';
import { Validators, FormControl } from '@angular/forms';
import { BasicCustomValidators } from 'src/app/validators/basic-custom-validators';
import { UsuarioService } from '../usuarios/shared/services/usuario.service';
import { UsuarioAdicionar } from '../usuarios/adicionar/models/usuario-adicionar.model';
import { BaseReactiveFormComponent } from 'src/app/shared/components/bases-forms/base-reactive-form.component';
import * as $ from 'jquery';
import { EscolaridadeService } from '../escolaridades/shared/escolaridade.service';
import { Escolaridade } from '../escolaridades/shared/escolaridade.model';
import { BaseResult } from 'src/app/shared/models/base-result.model';

@Component({
  selector: 'app-registrar',
  templateUrl: './registrar.component.html'
})
export class RegistrarComponent extends BaseReactiveFormComponent<UsuarioAdicionar> implements OnInit, AfterViewInit {

  public _escoladidades = new Array<Escolaridade>();

  constructor(
    injector: Injector,
    private _usuarioService: UsuarioService,
    private _escolaridadeService: EscolaridadeService) {

    super(injector, new UsuarioAdicionar(), _usuarioService, UsuarioAdicionar.fromJson, UsuarioAdicionar.validacoes);
  }

  ngOnInit(): void {
    this.buildResourceForm();
    this.loadForm();
    this.carregarEscolaridade();

    $(document).ready(function () {
      $('#Nome').focus();
    });
  }

  protected buildResourceForm(): void {
    let senha = new FormControl('', [BasicCustomValidators.required, Validators.minLength(4), Validators.maxLength(10)]);
    let confirmacaoSenha = new FormControl('', [BasicCustomValidators.required, Validators.minLength(4), Validators.maxLength(10), BasicCustomValidators.equal(senha)]);

    this._resourceForm = this._formBuilder.group({
      Id: null,
      Nome: ['', [BasicCustomValidators.required, Validators.maxLength(60)]],
      Sobrenome: ['', [BasicCustomValidators.required, Validators.maxLength(60)]],
      Email: ['', [BasicCustomValidators.required, Validators.maxLength(100), BasicCustomValidators.email]],
      Senha: senha,
      ConfirmacaoSenha: confirmacaoSenha,
      DataNascimento: [null, BasicCustomValidators.required],
      Escolaridade: [null, BasicCustomValidators.required]
    });
  }

  carregarEscolaridade(): void {
    this.loading();
    this._escolaridadeService.getAll().subscribe((result: BaseResult) => {
      this.unLoading();

      this._escoladidades = result.Data;
    });
  }

  salvar(): void {
    if (this._resourceForm.invalid) {
      this._toastrService.error('Os dados requeridos do formulário estão inválido!', 'Formulário inválido');
      return;
    }

    const resource = this.resourceFormToEntity(this._resourceForm.value);

    this.loading();

    this._usuarioService.create(resource).subscribe((result: BaseResult) => {
      this.unLoading();
      this.actionsForSuccess(this._resourceForm, result, false);
      this._router.navigate(['/login']);
    }, (error) => {
      this.unLoading();
      this.actionsForError(error);
    });
  }
}
