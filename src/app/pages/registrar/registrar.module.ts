
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { RegistrarComponent } from './registrar.component';

const routes: Routes = [
  { path: '', component: RegistrarComponent, pathMatch: 'full'},
];

@NgModule({
  declarations: [
    RegistrarComponent
  ],
  imports: [
    SharedModule,
    RouterModule.forChild(routes)
  ]
})
export class RegistrarModule { }
