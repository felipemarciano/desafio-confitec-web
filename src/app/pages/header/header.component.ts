import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/internal/Subscription';
import { UsuarioToken } from '../usuarios/shared/models/usuario-token.model';
import { UsuarioTokenService } from '../usuarios/shared/services/usuario-token.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {

  public _usuario = new UsuarioToken();
  private _observerUser: Subscription;

  constructor(private _usuarioService: UsuarioTokenService) {
    this._usuario = _usuarioService.getUser();
  }

  ngOnInit(): void{
    this._observerUser = this._usuarioService._usuarioAutenticao.subscribe((usuario: UsuarioToken) => {
      this._usuario = usuario;
    });
  }

  ngOnDestroy(): void {
    this._observerUser.unsubscribe();
  }

}
