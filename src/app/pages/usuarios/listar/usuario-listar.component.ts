import { Usuario } from './../shared/models/usuario.model';
import { Injector, Component, OnInit } from '@angular/core';
import { UsuarioService } from '../shared/services/usuario.service';
import { BaseListComponent } from 'src/app/shared/components/bases-forms/base-list.component';
import { BaseResult } from 'src/app/shared/models/base-result.model';

@Component({
  selector: "app-usuario-listar",
  templateUrl: "./usuario-listar.component.html"
})
export class UsuarioListarComponent extends BaseListComponent<Usuario> implements OnInit {


  constructor(injector: Injector, private _usuarioService: UsuarioService) {
    super(_usuarioService, injector);

  }

  ngOnInit(): void{
    this.carregarUsuarios();
  }

  carregarUsuarios(): void {
    this.loading();
    this._usuarioService.search({}).subscribe((result: BaseResult) => {
      this.unLoading();

      this._listEntity = result.Data;
    });
  }
}
