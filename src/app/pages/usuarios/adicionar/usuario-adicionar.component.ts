import { EscolaridadeService } from '../../escolaridades/shared/escolaridade.service';
import { Escolaridade } from '../../escolaridades/shared/escolaridade.model';
import { BaseReactiveFormComponent } from 'src/app/shared/components/bases-forms/base-reactive-form.component';
import { UsuarioAdicionar } from './models/usuario-adicionar.model';
import { UsuarioService } from '../shared/services/usuario.service';
import { Component, Injector, OnInit } from '@angular/core';
import { FormControl, Validators } from "@angular/forms";
import { BasicCustomValidators } from 'src/app/validators/basic-custom-validators';
import * as $ from 'jquery';
import { BaseResult } from 'src/app/shared/models/base-result.model';

@Component({
  selector: "app-usuario-adicionar",
  templateUrl: "./usuario-adicionar.component.html"
})
export class UsuarioAdicionarComponent extends BaseReactiveFormComponent<UsuarioAdicionar> implements OnInit {

  public _escoladidades = new Array<Escolaridade>();

  constructor(
    injector: Injector,
    private _usuarioService: UsuarioService,
    private _escolaridadeService: EscolaridadeService) {

    super(injector, new UsuarioAdicionar(), _usuarioService, UsuarioAdicionar.fromJson, UsuarioAdicionar.validacoes);
  }

  ngOnInit(): void {
    this.buildResourceForm();
    this.loadForm();
    this.carregarEscolaridade();

    $(document).ready(function () {
      $('#Nome').focus();
    });

  }

  protected buildResourceForm(): void {
    let senha = new FormControl('', [BasicCustomValidators.required, Validators.minLength(4), Validators.maxLength(10)]);
    let confirmacaoSenha = new FormControl('', [BasicCustomValidators.required, Validators.minLength(4), Validators.maxLength(10), BasicCustomValidators.equal(senha)]);

    this._resourceForm = this._formBuilder.group({
      Id: null,
      Nome: ['', [BasicCustomValidators.required, Validators.maxLength(60)]],
      Sobrenome: ['', [BasicCustomValidators.required, Validators.maxLength(60)]],
      Email: ['', [BasicCustomValidators.required, Validators.maxLength(100), BasicCustomValidators.email]],
      Senha: senha,
      ConfirmacaoSenha: confirmacaoSenha,
      DataNascimento: [null, BasicCustomValidators.required],
      Escolaridade: [null, BasicCustomValidators.required]
    });
  }

  carregarEscolaridade(): void {
    this.loading();
    this._escolaridadeService.getAll().subscribe((result: BaseResult) => {
      this.unLoading();

      this._escoladidades = result.Data;
    });
  }
}
