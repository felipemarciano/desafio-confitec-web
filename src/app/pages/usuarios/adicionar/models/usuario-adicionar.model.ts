import { Usuario } from '../../shared/models/usuario.model';

export class UsuarioAdicionar extends Usuario {

  public Senha?: string;
  public ConfirmacaoSenha?: string;

  static fromJson(jsonData: any): UsuarioAdicionar {
    return Object.assign(new UsuarioAdicionar(), jsonData);
  }

  static validacoes() {
    return {
      Nome: {
        required: "Informe o nome!",
        maxlength: "Informe no máximo 60 caracteres para o nome!"
      },
      Sobrenome: {
        required: "Informe o sobrenome!",
        maxlength: "Informe no máximo 60 caracteres para o sobrenome!"
      },
      DataNascimento: {
        required: "Informe a data de nascimento!"
      },
      Email: {
        required: "Informe o email!",
        maxlength: "Informe no máximo 100 caracteres para o email!",
        email: "Email inválido!"
      },
      Senha: {
        required: "Informe a senha!",
        minlength: "Informe no mínimo 4 caracteres para a senha!",
        maxlength: "Informe no máximo 10 caracteres para a senha!"
      },
      ConfirmacaoSenha: {
        required: "Informe a confirmação de senha!",
        minlength: "Informe no mínimo 4 caracteres para a confirmação de senha!",
        maxlength: "Informe no máximo 10 caracteres para a confirmação de senha!",
        equal: "As senhas não conferem!"
      },
      Escolaridade: {
        required: "Informe a escolaridade!"
      }
    };
  }
}
