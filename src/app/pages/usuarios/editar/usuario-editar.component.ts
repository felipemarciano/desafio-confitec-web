import { EscolaridadeService } from '../../escolaridades/shared/escolaridade.service';
import { Escolaridade } from '../../escolaridades/shared/escolaridade.model';
import { BaseReactiveFormComponent } from 'src/app/shared/components/bases-forms/base-reactive-form.component';
import { UsuarioService } from '../shared/services/usuario.service';
import { Component, Injector, OnInit} from '@angular/core';
import { Validators } from "@angular/forms";
import { BasicCustomValidators } from 'src/app/validators/basic-custom-validators';
import * as $ from 'jquery';
import { BaseResult } from 'src/app/shared/models/base-result.model';
import { UsuarioEditar } from './models/usuario-editar.model';
import { isNullOrWhiteSpace } from 'src/app/shared/modules/help-module';
import * as moment from 'moment';

@Component({
  selector: "app-usuario-editar",
  templateUrl: "./usuario-editar.component.html"
})
export class UsuarioEditarComponent extends BaseReactiveFormComponent<UsuarioEditar> implements OnInit {

  public _escoladidades = new Array<Escolaridade>();

  constructor(
    injector: Injector,
    private _usuarioService: UsuarioService,
    private _escolaridadeService: EscolaridadeService) {

    super(injector, new UsuarioEditar(), _usuarioService, UsuarioEditar.fromJson, UsuarioEditar.validacoes);
  }

  ngOnInit(): void {
    this.buildResourceForm();
    this.loadForm();
    $(document).ready(function () {
      $('#Nome').focus();
    });
    this.carregarEscolaridade();
  }

  protected buildResourceForm(): void {

    this._resourceForm = this._formBuilder.group({
      Id: null,
      Nome: ['', [BasicCustomValidators.required, Validators.maxLength(60)]],
      Sobrenome: ['', [BasicCustomValidators.required, Validators.maxLength(60)]],
      Email: ['', [BasicCustomValidators.required, Validators.maxLength(100), BasicCustomValidators.email]],
      DataNascimento: [null, BasicCustomValidators.required],
      Escolaridade: [null, BasicCustomValidators.required]
    });
  }

  carregarEscolaridade(): void {
    this.loading();
    this._escolaridadeService.getAll().subscribe((result: BaseResult) => {
      this.unLoading();

      this._escoladidades = result.Data;
    });
  }

  loadForm(): void {

    this._route.paramMap.subscribe((params) => {
      const id = params.get('id');
      if (isNullOrWhiteSpace(id)) {
        return;
      }

      this.loading();
      this._resourceService.getById(id).subscribe((result: BaseResult) => {
        this.unLoading();
        this._entity = result.Data;
        this._entity.DataNascimento = new Date(moment(this._entity.DataNascimento).format());
        this._resourceForm.patchValue(this._entity);
      }, (error) => {
        this.unLoading();
        this.actionsForError(error);
      });
    });
  }
}
