import { Usuario } from './../../shared/models/usuario.model';

export class UsuarioEditar extends Usuario {

  static fromJson(jsonData: any): UsuarioEditar {
    return Object.assign(new UsuarioEditar(), jsonData);
  }

  static validacoes() {
    return {
      Nome: {
        required: "Informe o nome!",
        maxlength: "Informe no máximo 60 caracteres para o nome!"
      },
      Sobrenome: {
        required: "Informe o sobrenome!",
        maxlength: "Informe no máximo 60 caracteres para o sobrenome!"
      },
      DataNascimento: {
        required: "Informe a data de nascimento!"
      },
      Email: {
        required: "Informe o email!",
        maxlength: "Informe no máximo 100 caracteres para o email!",
        email: "Email inválido!"
      },
      Escolaridade: {
        required: "Informe a escolaridade!"
      }
    };
  }
}
