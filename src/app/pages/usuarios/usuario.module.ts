import { UsuarioAdicionarComponent } from './adicionar/usuario-adicionar.component';
import { UsuarioListarComponent } from './listar/usuario-listar.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { BreadcrumbParent } from '../breadcrumb/models/breadcrumb-parent.model';
import { UsuarioEditarComponent } from './editar/usuario-editar.component';
import { AuthUsuarioService } from './shared/services/auth.usuario.service';

const breadcrumb: BreadcrumbParent[]  = [{id: 1, name: 'Usuários', routerLink: '/usuarios'}];

const routes: Routes = [
  { path: '', component: UsuarioListarComponent, canActivate : [AuthUsuarioService], pathMatch: 'full', data: { breadcrumb: {name: breadcrumb[0].name} } },
  { path: 'adicionar', component: UsuarioAdicionarComponent, canActivate : [AuthUsuarioService], data: { breadcrumb: {name:'Adicionar', parents: breadcrumb } } },
  { path: ':id/editar', component: UsuarioEditarComponent, canActivate : [AuthUsuarioService], data: { breadcrumb: {name:'Editar', parents: breadcrumb } } }
];

@NgModule({
  declarations: [
    UsuarioListarComponent,
    UsuarioAdicionarComponent,
    UsuarioEditarComponent
  ],
  imports: [
    SharedModule,
    RouterModule.forChild(routes)
  ],
  exports:[
    UsuarioListarComponent,
    UsuarioAdicionarComponent,
    UsuarioEditarComponent
  ]
})
export class UsuarioModule { }
