import { Injectable, Injector, Inject } from "@angular/core";
import { CanActivate } from "@angular/router";
import { ActivatedRouteSnapshot, Router } from "@angular/router";
import { UsuarioTokenService } from "./usuario-token.service";

@Injectable()
export class AuthUsuarioService implements CanActivate {
    path: ActivatedRouteSnapshot[];
    route: ActivatedRouteSnapshot;
    private _usuarioTokenService: UsuarioTokenService;
    private _router: Router;

    constructor(@Inject(Injector) private injector: Injector) {
        this._usuarioTokenService = injector.get(UsuarioTokenService);
        this._router = injector.get(Router);
    }

    canActivate(routeAC: ActivatedRouteSnapshot): boolean {
        if(!this._usuarioTokenService.isAuthenticated()){
            this._router.navigate(['/logout']);
        }

        return true;
    }
}
