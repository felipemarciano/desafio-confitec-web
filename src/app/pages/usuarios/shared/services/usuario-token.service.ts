import { UsuarioToken } from './../models/usuario-token.model';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { TokenLogin } from 'src/app/pages/usuarios/shared/models/token-login.model';
import { Router } from '@angular/router';

@Injectable()
export class UsuarioTokenService {

  public _usuarioAutenticao = new Subject<UsuarioToken>();

  constructor(private _router: Router) {

  }

  public setLocalStorage(tokenLogin: TokenLogin) {
    localStorage.setItem("conf.token", tokenLogin.Token);
    localStorage.setItem("conf.usuario", JSON.stringify(tokenLogin.Usuario));
    this._usuarioAutenticao.next(tokenLogin.Usuario);
  }

  public getUserToken(): string {
    return localStorage.getItem("conf.token");
  }

  public getUser(): UsuarioToken {
    return JSON.parse(localStorage.getItem("conf.usuario"));
  }

  public removeUserToken() {
    localStorage.removeItem("conf.token");
    localStorage.removeItem("conf.usuario");
    this._usuarioAutenticao.next(null);
  }

  public isAuthenticated(): boolean {
    const user = this.getUser();
    const usuarioLogado = user !== null;

    this._usuarioAutenticao.next(user);

    return usuarioLogado;
  }

  public logOut(): void {
    this._router.navigate(['/logout']);
  }
}
