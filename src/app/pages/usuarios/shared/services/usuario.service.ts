import { Injector, Injectable } from '@angular/core';
import { BaseService } from 'src/app/services/base.service';
import { environment } from 'src/environments/environment';
import { BaseResult } from 'src/app/shared/models/base-result.model';
import { Observable } from 'rxjs';
import { timeout, map, retry } from "rxjs/operators";
import { Usuario } from '../models/usuario.model';

@Injectable()
export class UsuarioService extends BaseService<Usuario> {

  private _apiLogin = environment.apiUrl + 'account/login';
  private _apiLogOUt = environment.apiUrl + 'account/logout';

  private _headers = {
    "Content-Type": "application/json; charset=utf-8"
  };

  constructor(injector: Injector) {
    super("usuarios", injector, Usuario.fromJson);
  }

  login(resource: any): Observable<BaseResult> {
    return this._http
      .post(this._apiLogin, resource, { headers: this._headers }).pipe(
        map(BaseResult.fromJson),
        timeout(environment.timeOutAPI),
        retry(1)
      );
  }

  logOut(): Observable<any> {
    return this._http
      .get(this._apiLogOUt, { headers: this.gerarHeader() }).pipe(
        map((data: any) => data),
        timeout(environment.timeOutAPI)
      );
  }
}
