import { BaseModel } from '../../../../shared/models/base.model';

export class UsuarioToken extends BaseModel{
    public Id?: string;
    public Nome: string;
    public Email: string;

    static fromJson(jsonData: any): UsuarioToken {
        return Object.assign(new UsuarioToken(), jsonData);
    }
}
