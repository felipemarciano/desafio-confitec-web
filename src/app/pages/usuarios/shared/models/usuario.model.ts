import { BaseModel } from 'src/app/shared/models/base.model';

export class Usuario extends BaseModel {

  public Nome: string;
  public Sobrenome: string;
  public Email: string;
  public Escolaridade: number;
  public DataNascimento: Date
  public EscolaridadeDescricao?: string;

  static fromJson(jsonData: any): Usuario {
    return Object.assign(new Usuario(), jsonData);
  }
}
