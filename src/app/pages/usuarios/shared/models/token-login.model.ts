import { UsuarioToken } from "./usuario-token.model";

export class TokenLogin{
    public Token: string;
    public ExpiraEm: number;
    public Usuario: UsuarioToken;
}
