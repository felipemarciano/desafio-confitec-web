import { UsuarioTokenService } from './pages/usuarios/shared/services/usuario-token.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { UsuarioToken } from './pages/usuarios/shared/models/usuario-token.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

  public _usuarioAutenticado: boolean;
  private _observerUser: Subscription;

  constructor(private _usuarioService: UsuarioTokenService) {

  }

  ngOnInit(): void{
    this._observerUser = this._usuarioService._usuarioAutenticao.subscribe((usuario: UsuarioToken) => {
      this._usuarioAutenticado = usuario !== null;
    });
  }

  ngOnDestroy(): void {
    this._observerUser.unsubscribe();
  }
}
