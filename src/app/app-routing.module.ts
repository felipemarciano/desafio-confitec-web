import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AcessoNegadoComponent } from './pages/acesso-negado/acesso-negado.component';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { LogOutComponent } from './pages/logout/logout.component';
import { AuthUsuarioService } from './pages/usuarios/shared/services/auth.usuario.service';

const routes: Routes = [
  {path: '', component: HomeComponent, canActivate : [AuthUsuarioService], pathMatch: 'full'},
  {path: 'login', component: LoginComponent},
  {path: 'registrar', loadChildren: () => import('./pages/registrar/registrar.module').then(m => m.RegistrarModule)},
  {path: 'usuarios', loadChildren: () => import('./pages/usuarios/usuario.module').then(m => m.UsuarioModule)},
  {path: 'acesso-negado', component: AcessoNegadoComponent},
  {path: 'sobre', loadChildren: () => import('./pages/sobre/sobre.module').then(m => m.SobreModule)},
  {path: 'logout', component: LogOutComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
