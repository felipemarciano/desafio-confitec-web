import { Injector } from "@angular/core";
import { Observable } from "rxjs";
import { timeout, map } from "rxjs/operators";
import { environment } from "src/environments/environment";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { ToastrService } from "ngx-toastr";
import { Router } from "@angular/router";
import { BaseModel } from '../shared/models/base.model';
import { BaseResult } from '../shared/models/base-result.model';
import { UsuarioTokenService } from "../pages/usuarios/shared/services/usuario-token.service";

export abstract class BaseService<T extends BaseModel> {
  protected _http: HttpClient;
  protected _apiPath: string;

  private _usuarioTokenService: UsuarioTokenService;
  private _retorno: BaseResult;
  private _router: Router;
  protected _mensagemErroConexaoServidor =
    `Ops! Não conseguimos processar os dados no servidor. Verifique a conexão a internet,
    caso o erro persista entre em contato com o suporte.`;

  constructor(
    protected _pathResource: string,
    protected _injector: Injector,
    protected jsonDataToResourceFn: (jsonData: any) => T
  ) {
    this._apiPath = `${environment.apiUrl}${this._pathResource}`;
    this._http = _injector.get(HttpClient);
    this._router = _injector.get(Router);
    this._usuarioTokenService = _injector.get(UsuarioTokenService);


  }

  protected gerarHeader(): HttpHeaders {
    const header = new HttpHeaders({
      "Content-Type": "application/json; charset=utf-8",
      "Authorization": `Bearer ${this._usuarioTokenService.getUserToken()}`
    })
    return header;
  }

  getAll(): Observable<BaseResult | any> {
    return this._http.get(this._apiPath, { headers: this.gerarHeader() }).pipe(
      map(this.jsonDataToResource.bind(this)),
      timeout(environment.timeOutAPI)
    );
  }

  getById(id: any): Observable<BaseResult | any> {
    const url = `${this._apiPath}/${id}`;

    return this._http.get(url, { headers: this.gerarHeader() }).pipe(
      map(this.jsonDataToResource.bind(this)),
      timeout(environment.timeOutAPI)
    );
  }

  create(resource: any): Observable<BaseResult | any> {
    return this._http.post(this._apiPath, resource, { headers: this.gerarHeader() }).pipe(
      map(this.jsonDataToResource.bind(this)),
      timeout(environment.timeOutAPI)
    );
  }

  update(resource: any): Observable<BaseResult | any> {
    const url = `${this._apiPath}`;

    return this._http.put(url, resource, { headers: this.gerarHeader() }).pipe(
      map(this.jsonDataToResource.bind(this)),
      timeout(environment.timeOutAPI)
    );
  }

  delete(id: any): Observable<BaseResult | any> {
    const url = `${this._apiPath}/${id}`;

    return this._http.delete(url, { headers: this.gerarHeader() }).pipe(
      map(this.jsonDataToResource.bind(this)),
      timeout(environment.timeOutAPI)
    );
  }

  search(resource: any): Observable<BaseResult | any> {
    const url = `${this._apiPath}/pesquisar`;
    return this._http.post(url, resource, { headers: this.gerarHeader() }).pipe(
      map(this.jsonDataToResource.bind(this)),
      timeout(environment.timeOutAPI)
    );
  }

  protected get _toastr(): ToastrService {
    return this._injector.get(ToastrService);
  }

  protected jsonDataToResources(jsonData: any[]): T[] {
    const resources: T[] = [];
    jsonData.forEach(element =>
      resources.push(this.jsonDataToResourceFn(element))
    );
    return resources;
  }

  protected jsonDataToResource(jsonData: any): T {
    return this.jsonDataToResourceFn(jsonData);
  }

  protected extractData(response: any) {
    return response.Data || {};
  }

  protected handleError(response: BaseResult | any) {
    if (response instanceof BaseResult) {
      if (response.HttpStatus != 200 && response.HttpStatus != 400) {
        this._retorno = new BaseResult();
        this._retorno.Message = this._mensagemErroConexaoServidor;
        this._retorno.Success = false;
        return Observable.throw(this._retorno);
      }
    }
    return Observable.throw(
      response.json() || this._mensagemErroConexaoServidor
    );
  }
}
