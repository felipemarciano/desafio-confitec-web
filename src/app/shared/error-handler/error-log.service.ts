import { NgxSpinnerService } from 'ngx-spinner';
import { isNullOrUndefined } from 'src/app/shared/modules/help-module';
import { Injectable, Injector, Inject } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { BaseResult } from '../models/base-result.model';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { UsuarioTokenService } from 'src/app/pages/usuarios/shared/services/usuario-token.service';

@Injectable()
export class ErrorLogService {

  constructor(@Inject(Injector) private injector: Injector) {

  }

  protected _toastr(): ToastrService {
    return this.injector.get(ToastrService);
  }

  protected _spinner(): NgxSpinnerService {
    return this.injector.get(NgxSpinnerService);
  }

  protected _usuarioService(): UsuarioTokenService {
    return this.injector.get(UsuarioTokenService);
  }

  protected _router(): Router {
    return this.injector.get(Router);
  }

  private validarBaseResult(responseError: any): boolean {
    let result: BaseResult;
    if (responseError && responseError.Errors) {
      result = Object.assign({}, new BaseResult(), responseError);
    }
    else if (responseError.error && responseError.error.Errors) {
      result = Object.assign({}, new BaseResult(), responseError.error);
    }

    if (isNullOrUndefined(result)) {
      return false;
    }

    if (result.Errors.length === 0) {
      this._toastr().error("Houve um erro durante o processamento da requisição. Por favor, tente mais tarde!");
      return false;
    }
    else if (result.HttpStatus === 404) {
      this._toastr().warning(result.Errors.toString());
    }
    else {
      this._toastr().error(result.Errors.toString());
    }
    return true;
  }

  private validarHttpErrorResponse(responseError: any): boolean {
    if (responseError instanceof HttpErrorResponse) {
      switch (responseError.status) {
        case 401:
          this._usuarioService().logOut();
          return false;

        case 403:
          this._router().navigate(['/acesso-negado']);
          break;

        case 404:
          this._toastr().error('Recurso não encontrado!');
          break;

        default:
          if (responseError.message.toLowerCase().includes('unknown url') ||
            responseError.statusText.toLowerCase().includes('unknown error')) {
            this._toastr().error(`Houve um problema de comunicação com o servidor!
                             Tente mais tarde, caso o erro persista entre em contato conosco!`, "Erro");
            break;
          }
      }
      return true;
    }
    return false;
  }

  logError(responseError: any) {
    this._spinner().hide();

    const baseResultValido = this.validarBaseResult(responseError);
    if (baseResultValido) {
      throw responseError;
    }

    const httpErroValido = this.validarHttpErrorResponse(responseError);
    if (httpErroValido) {
      throw responseError;
    }
    let mensagem = '';

    if (responseError.name === "TimeoutError") {
      mensagem = 'Tempo limite esgotado! Por favor, tente mais tarde!';
      this._toastr().error(mensagem);
    }
    else if (responseError instanceof TypeError) {
      mensagem = `Message: ${responseError.message} \n Stack: ${responseError.stack}`;
      this._toastr().error(mensagem);
    } else if (responseError instanceof Error) {
      mensagem = `Message: ${responseError.message} \n Stack: ${responseError.stack}`;
      this._toastr().error(mensagem);
    } else if (responseError instanceof ErrorEvent) {
      mensagem = 'Message: ${responseError.message}';
      this._toastr().error(mensagem);
    } else {
      mensagem = `Message: ${responseError.message} \n Stack: ${responseError.stack}`;
      this._toastr().error(mensagem);
    }
    console.error(mensagem);
    throw responseError;
  }
}
