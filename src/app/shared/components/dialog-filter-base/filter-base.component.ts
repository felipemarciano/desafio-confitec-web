import { isNullOrUndefined } from 'src/app/shared/modules/help-module';
import { FilterBase } from './filter/filter-base';
import { Component, Inject, ViewEncapsulation } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-filter-base',
  templateUrl: './filter-base.component.html',
  encapsulation: ViewEncapsulation.None
})
export class FilterBaseDialogComponent {
  constructor(private _dialogRef: MatDialogRef<FilterBase>,
    @Inject(MAT_DIALOG_DATA) public _filter: FilterBase) {
    if (_filter === null) {
      _filter = new FilterBase();
    }
  }

  confirmar(): void {
    if (isNullOrUndefined(this._filter.Ativo)) {
      this._filter.Ativo = false;
    }
    this._dialogRef.close(this._filter);
  }

  cancelar(): void {
    this._dialogRef.close(null);
  }
}
