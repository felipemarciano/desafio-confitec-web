export class FilterBase {
  public CodigoExterno?: string;
  public DataAtualizacaoInicio?: Date;
  public DataAtualizacaoTermino?: Date;
  public DataCadastroInicio?: Date;
  public DataCadastroTermino?: Date;
  public Descricao?: string;
  public Ativo: boolean = true;
}
