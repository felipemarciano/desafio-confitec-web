import { Component, ViewEncapsulation } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-dialog-confirm',
  templateUrl: './dialog-confirm.component.html',
  encapsulation: ViewEncapsulation.None
})
export class ConfirmDialogComponent {

  constructor(private _dialogRef: MatDialogRef<boolean>) {

  }

  confirmar(): void {
    this._dialogRef.close(true);
  }

  cancelar(): void {
    this._dialogRef.close(false);
  }
}
