import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { GlobalErrorHandler } from '../../error-handler/global-error-handler';
import { BaseService } from 'src/app/services/base.service';
import { BaseModel } from '../../models/base.model';
import { Injector } from '@angular/core';
import { BaseResult } from '../../models/base-result.model';
import { HttpErrorResponse } from '@angular/common/http';
import { GenericValidator } from '../../utils/generic-validator';
import { MatDialog } from '@angular/material/dialog';

export abstract class BaseForm<T extends BaseModel> {

  protected _formBuilder: FormBuilder;
  protected _route: ActivatedRoute;
  protected _router: Router;
  protected _dialog: MatDialog;
  public _messageValidator: { [key: string]: string } = {};
  public _toastrService: ToastrService;
  private _ngxSpinnerBaseService: NgxSpinnerService;
  protected _globalErrorHandler: GlobalErrorHandler;
  protected _gerericValidator: GenericValidator;

  constructor(private injectorBase: Injector,
    protected resourceFormToEntity: (jsonData) => T,
    protected _resourceService: BaseService<T>,
    protected formValidate: () => { [key: string]: { [key: string]: string } }) {

    this._formBuilder = this.injectorBase.get(FormBuilder);
    this._route = this.injectorBase.get(ActivatedRoute);
    this._router = this.injectorBase.get(Router);
    this._toastrService = this.injectorBase.get(ToastrService);
    this._globalErrorHandler = this.injectorBase.get(GlobalErrorHandler);
    this._ngxSpinnerBaseService = this.injectorBase.get(NgxSpinnerService);
    this._dialog = this.injectorBase.get(MatDialog);
    this._gerericValidator = new GenericValidator(this.formValidate());
  }

  submitForm(form: FormGroup, redirect: boolean = true, afterSubmitRouterNavigate: string = '') {
    if (form.invalid) {
      this._toastrService.error('Os dados requeridos do formulário estão inválido!', 'Formulário inválido');
      return;
    }

    if (form.get('Id').value === null)
      this.createResource(form, redirect, afterSubmitRouterNavigate);
    else
      this.updateResource(form, redirect, afterSubmitRouterNavigate);
  }

  protected loading(): void{
    this._ngxSpinnerBaseService.show();
  }

  protected unLoading(): void{
    this._ngxSpinnerBaseService.hide();
  }

  protected createResource(form: FormGroup, redirect: boolean = true, afterSubmitRouterNavigate: string = '') {
    const resource: T = this.resourceFormToEntity(form.value);

    this.loading();

    this._resourceService.create(resource).subscribe((result: BaseResult) => {
      this.actionsForSuccess(form, result, redirect, afterSubmitRouterNavigate);
      this.unLoading();
    }, (error) => {
      this.unLoading();
      this.actionsForError(error);
    });
  }

  protected updateResource(form: FormGroup, redirect: boolean = true, afterSubmitRouterNavigate: string = '') {
    const resource: T = this.resourceFormToEntity(form.value);

    this.loading();

    this._resourceService.update(resource).subscribe((result: BaseResult) => {
      this.actionsForSuccess(form, result, redirect, afterSubmitRouterNavigate);
      this.unLoading();
    }, (error) => {
      this.unLoading();
      this.actionsForError(error);
    });
  }

  protected actionsForMessages(response: BaseResult): boolean {
    if (response.Success) {
      this._toastrService.success(response.Message);
    } else {
      this._toastrService.error(response.Errors.toString());
      return false;
    }
    return true;
  }

  protected actionsForSuccess(form: FormGroup, response: BaseResult, redirect: boolean = true, afterSubmitRouterNavigate: string = ''): void {

    const retornoOk = this.actionsForMessages(response);
    if (!retornoOk) {
      return;
    }

    form.reset();

    if (!redirect) {
      return;
    }

    setTimeout(() => {
      let baseComponentPath: string = '';
      if (this._route.snapshot.parent != null) {
        baseComponentPath = '/' + this._route.snapshot.parent.url[0].path;
      }

      this._router.navigateByUrl(baseComponentPath, { skipLocationChange: true }).then(() => {
        if (afterSubmitRouterNavigate === '' && this._route.snapshot.data.breadcrumb.name !== 'Editar') {
          afterSubmitRouterNavigate = '/adicionar';
        }
        this._router.navigate([
          baseComponentPath + '/' + afterSubmitRouterNavigate,
        ]);
      });
    }, 1000);
  }

  protected actionsForError(responseError: HttpErrorResponse | any) {
    this._globalErrorHandler.handleError(responseError);
  }

  protected abstract buildResourceForm(): void;

}
