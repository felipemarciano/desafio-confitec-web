import { BaseResult } from 'src/app/shared/models/base-result.model';
import { BaseService } from 'src/app/services/base.service';
import { Injector } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { BaseModel } from "src/app/shared/models/base.model";
import { ToastrService } from "ngx-toastr";
import { HttpErrorResponse } from "@angular/common/http";
import { NgxSpinnerService } from 'ngx-spinner';
import { MatDialog } from '@angular/material/dialog';
import { GlobalErrorHandler } from '../../error-handler/global-error-handler';
import { FilterBase } from '../dialog-filter-base/filter/filter-base';
import { ConfirmDialogComponent } from '../dialog-confirm/dialog-confirm.component';
import { FilterBaseDialogComponent } from '../dialog-filter-base/filter-base.component';
import { DialogUtil } from '../../utils/dialog-util';
import { isNullOrUndefined } from 'src/app/shared/modules/help-module';

export abstract class BaseListComponent<T extends BaseModel> {

  public _listEntity: T[];
  protected _route: ActivatedRoute;
  protected _router: Router;
  protected _toastrService: ToastrService;
  protected _submittingForm: boolean;
  private _globalErrorHandler: GlobalErrorHandler;
  protected _dialog: MatDialog;
  protected _dialogUtil: DialogUtil;
  private _ngxSpinnerService: NgxSpinnerService;
  private _filterList = new FilterBase();

  constructor(
    protected _resourceService: BaseService<T>,
    protected _injector: Injector,
  ) {
    this._route = this._injector.get(ActivatedRoute);
    this._router = this._injector.get(Router);
    this._toastrService = this._injector.get(ToastrService);
    this._globalErrorHandler = this._injector.get(GlobalErrorHandler);
    this._listEntity = new Array<T>();
    this._ngxSpinnerService = this._injector.get(NgxSpinnerService);
    this._dialog = this._injector.get(MatDialog);
    this._dialogUtil = this._injector.get(DialogUtil);
  }

  protected loading(): void {
    this._ngxSpinnerService.show();
  }

  protected unLoading(): void {
    this._ngxSpinnerService.hide();
  }

  deleteResource(resource: T) {
    if (resource.Id == null) {
      this._toastrService.error('Não é possível excluir o item atual! Tente mais tarde.');
      return;
    }

    const dialogRef = this._dialog.open(ConfirmDialogComponent, { disableClose: true });

    dialogRef.afterClosed().subscribe((confirm: boolean) => {
      this.loading();

      if (confirm === true) {
        this._resourceService.delete(resource.Id).subscribe(
          (result: BaseResult) => {
            this.unLoading();

            if (result.Success) {
              this._listEntity = this._listEntity.filter(element => element !== resource);
              this._toastrService.success(result.Message);
            } else {
              this._toastrService.error(result.Errors.toString());
            }
          },
          responseError => {
            this.unLoading();
            const resultError: BaseResult = Object.assign({}, new BaseResult(), responseError.error);
            this._toastrService.error(resultError.Errors.toString());
          }
        );
      }
      else {
        this.unLoading();
      }
    });
  }


  protected actionsForError(responseError: HttpErrorResponse | any) {
    this._submittingForm = false;

    this._globalErrorHandler.handleError(responseError);
  }

  public filterBase(): void {
    let dialogRef = this._dialogUtil.addObjectModal(FilterBaseDialogComponent, 40, this._filterList);

    dialogRef.afterClosed().subscribe((filter: FilterBase) => {

      if (filter !== null) {
        this._filterList = filter;
        this.loading();

        this._resourceService.search(filter)
          .subscribe((result: BaseResult) => {
            this.unLoading();
            this._listEntity = result.Data as T[];
          },
            error => this.actionsForError(error));
      }
    });
  }
}
