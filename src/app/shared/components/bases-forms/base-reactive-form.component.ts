import { BaseResult } from 'src/app/shared/models/base-result.model';
import { ViewChildren, ElementRef, AfterViewInit, Injector, Directive } from '@angular/core';
import { FormGroup, FormControlName } from '@angular/forms';
import { Observable, fromEvent, merge } from 'rxjs';
import { BaseService } from 'src/app/services/base.service';
import { BaseForm } from './base-form.component';
import { BaseModel } from '../../models/base.model';
import { isNullOrWhiteSpace } from 'src/app/shared/modules/help-module';

@Directive()
export abstract class BaseReactiveFormComponent<T extends BaseModel> extends BaseForm<T> implements AfterViewInit {

  @ViewChildren(FormControlName, { read: ElementRef }) protected formInputElements: ElementRef[];

  public _resourceForm: FormGroup;
  protected _errors: any[] = [];

  constructor(private _injector: Injector,
    public _entity: T,
    protected _resourceService: BaseService<T>,
    protected resourceFormToEntity: (jsonData) => T,
    protected formValidate: () => { [key: string]: { [key: string]: string } }) {

    super(_injector, resourceFormToEntity, _resourceService, formValidate);

  }

  ngAfterViewInit() {
    let controlBlurs: Observable<any>[] = this.formInputElements.map((formControl: ElementRef) => fromEvent(formControl.nativeElement, 'blur'));

    merge(...controlBlurs).subscribe((value) => {
      this._messageValidator = this._gerericValidator.processMessages(this._resourceForm);
    });
  }

  protected loadForm(): void {

    this._route.paramMap.subscribe((params) => {
      const id = params.get('id');
      if (isNullOrWhiteSpace(id)) {
        return;
      }

      this.loading();
      this._resourceService.getById(id).subscribe((result: BaseResult) => {
        this.unLoading();
        this._entity = result.Data as T;
        this._resourceForm.patchValue(this._entity);
      }, (error) => {
        this.unLoading();
        this.actionsForError(error);
      });
    });
  }
}
