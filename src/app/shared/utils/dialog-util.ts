import { Injectable, TemplateRef } from '@angular/core';
import { ComponentType } from '@angular/cdk/portal';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';
import { Guid } from 'guid-typescript';

@Injectable()
export class DialogUtil {

  constructor(private _dialog: MatDialog) {

  }

  public addObjectModal<C>(component: ComponentType<C> | TemplateRef<C>, percentWidth: number = 40,
    parameterData: any = 'modal'): MatDialogRef<C, any> {

    let componentId = Guid.create().toString();

    let dialog = this._dialog.open<C>(component,
      {
        id: componentId,
        disableClose: true,
        panelClass: `modal-w-${percentWidth}`,
        data: parameterData
      });

    return dialog;
  }
}
