export class ValidarPeriodo{
    static validar(dataInicio: Date, dataTermino: Date): string[]{
        let result = new Array<string>();
        if(dataInicio == null)
            result.push('Data de início inválida!');

        if(dataTermino == null)
            result.push('Data de término inválida!');

        if(dataTermino < dataInicio)
            result.push('A data de término não pode ser menor do que a data de início!');

        return result;
    }
}