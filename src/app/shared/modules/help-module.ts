export function isNullOrWhiteSpace(object: string): boolean {
  return object === undefined || object === null || object.length === 0;
}

export function isNullOrUndefined(object: any): boolean {
  return object === undefined || object === null;
}

