import { MatDialogRef } from '@angular/material/dialog';

declare global{
  interface Object {
    containerShow(): void;
    containerHide(): void;
  }
}

MatDialogRef.prototype.containerShow = function(): void {
  this.removePanelClass('container-hidden');
}

MatDialogRef.prototype.containerHide = function(): void{
  this.addPanelClass('container-hidden');
}
