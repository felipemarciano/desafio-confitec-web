interface Number{
    toNumberPtBr(places: number): number;
}

Number.prototype.toNumberPtBr = function(places: number): number {
    if(this === null || this === undefined){
        return 0;
    }
    return Number(Number(this).toFixed(places));
}