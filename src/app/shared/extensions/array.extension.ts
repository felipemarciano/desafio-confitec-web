
interface Array<T> {
  orderBy(property: string): void;
  orderByDesc(property: string): void;
  add(id: string, propertyName: string, valuePropertyName: string): void;
  addRange(list: Array<T>): void;
}

Array.prototype.orderBy = function (property: string): void {
  this.sort((f1, f2) => {
    return +(f1[property] > f2[property]) || +(f1[property] === f2[property]) - 1;
  });
}

Array.prototype.orderByDesc = function (property: string): void {
  this.sort((f2, f1) => {
    return +(f1[property] > f2[property]) || +(f1[property] === f2[property]) - 1;
  });
}

Array.prototype.add = function (id: string, propertyName: string, valuePropertyName: string): void {
  const obj = { Id: id, [propertyName]: valuePropertyName };
  this.push(obj);
}

Array.prototype.addRange = function(list: any[]): void {
  for(let i = 0; i < list.length; i++){
    this.push(list[i]);
  }
}
