export class BaseResult{
  public Data: any;
  public Errors:string[]=[];
  public HttpStatus: number;
  public Message: string;
  public Success: boolean;

  static fromJson(jsonData: any): BaseResult {
      return Object.assign(new BaseResult(), jsonData);
  }
}
